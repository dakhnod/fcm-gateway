import quart
import argparse
import aiohttp
import time
import os
import types

try:
    fcm_client_id = os.environ['FCM_CLIENT_ID']
    fcm_client_secret = os.environ['FCM_CLIENT_SECRET']
    fcm_refresh_token = os.environ['FCM_REFRESH_TOKEN']
    fcm_project_id = os.environ['FCM_PROJECT_ID']
except KeyError:
    raise RuntimeError("Missing FCM_CLIENT_ID, FCM_CLIENT_SECRET, FCM_REFRESH_TOKEN or FCM_PROJECT_ID from env")

api_token = os.environ.get('API_TOKEN')

timeout = os.environ.get('TIMEOUT', 30)
if timeout is not None:
    timeout = int(timeout)

app = quart.Quart(__name__)
app.cli.allow_extra_args = True


@app.before_serving
async def app_setup():
    app.http = types.SimpleNamespace()
    app.auth = types.SimpleNamespace()
    app.http.client = aiohttp.ClientSession()
    app.cache = {}
    app.auth.token = None
    app.auth.expiration = None

def dict_filter(data, keys):
    return {key: data[key] for key in keys}

@app.post('/api/fcm/send')
async def api_send():
    data = await quart.request.json
    # data = dict_filter(data, ['type', 'to', 'data', 'notification'])
    to = data['message']['token']
    token = quart.request.headers.get('Authorization')
    throttled = (token is None) or (api_token is None) or (token != f'Bearer {api_token}')
    now = time.time()
    if throttled and (timeout is not None):
        try:
            time_last = app.cache[to]
            time_delta = now - time_last
            if time_delta <= timeout:
                return f'Please wait for {int(timeout - time_delta)} seconds', 429
        except KeyError:
            pass
        app.cache[to] = now


    if app.auth.token is None or now > app.auth.expiration:
        response = await app.http.client.post(
            'https://oauth2.googleapis.com/token',
            data={
                'grant_type': 'refresh_token',
                'client_id': fcm_client_id,
                'client_secret': fcm_client_secret,
                'refresh_token': fcm_refresh_token
            }
        )
        response = await response.json()
        app.auth.token = response['access_token']
        app.auth.expiration = now + response['expires_in'] - 10 # 10 seconds to account for request delay

    response = await app.http.client.post(
        f'https://fcm.googleapis.com/v1/projects/{fcm_project_id}/messages:send',
        json=data,
        headers={
            'Authorization': f'Bearer {app.auth.token}',
            'Content-Type': 'application/json'
        }
    )
    return quart.Response(
        await response.text(),
        response.status,
        {'Content-Type': 'application/json'}
    )
