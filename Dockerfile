FROM python:alpine

EXPOSE 5000

WORKDIR /app

COPY requirements.txt .
#RUN pip install -r requirements.txt
RUN pip install quart aiohttp argparse

COPY app.py .

CMD quart run --host 0.0.0.0
